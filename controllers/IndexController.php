<?php

namespace app\controllers;

use app\models\Blog;
use yii\data\Pagination;
use yii\web\HttpException;


class IndexController extends MainConntroller {

    public function actionIndex() {
        die('actionIndex');
        $query = Blog::find('id', 'title', 'except')->orderBy('id DESC');
        $pager = new Pagination(['totalCount' => $query->count(), 'pageSize' => 2, 'pageSizeParam' => false, 'forcePageParam' => false]);
        $posts = $query->offset($pager->offset)->limit($pager->limit)->all();

        return $this->render('index', compact('posts', 'pager'));
    }

    public function actionInner() {
        die('actionInner');
        $id = \Yii::$app->request->get(id);
        $post = Blog::findOne($id);
        if (empty($post)) {
            throw new HttpException(404, 'Запрашиваемая страница не найдена..');
        }
        
        return $this->render('inner', compact('post'));
    }

}
