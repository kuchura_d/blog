<?php

namespace app\modules\admin\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller {

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex() {
        die('actionIndex');
        if (!Yii::$app->user->isGuest) {
            print_r('guest');
            die;
            return $this->render('login');
        } else {
            return $this->render('index');
        }
    }

    public function loginAction() {
        die('loginAction');
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('index', compact('model'));
    }

    public function logoutAction() {
        die('logoutAction');
        Yii::$app->user->logout();

        return $this->render('login');
    }

}
