<?php if (!empty($posts)): ?>
    <?php foreach ($posts as $item): ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><a href="/inner/<?php echo $item->id; ?>"><?php echo $item->title; ?></a></h3>
            </div>
            <div class="panel-body"><?php echo $item->excerpt; ?></div>
        </div>
    <?php endforeach; ?>
    <?php echo \yii\widgets\LinkPager::widget(['pagination' => $pager]); ?>
<?php endif; ?>