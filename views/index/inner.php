<?php if (!empty($post)): ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo $post->title; ?></h3>
            </div>
            <div class="panel-body"><?php echo $post->text; ?></div>
        </div>
<?php endif; 