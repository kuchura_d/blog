<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /><script type="text/javascript">window.NREUM || (NREUM = {}), __nr_require = function(e, t, n){function r(n){if (!t[n]){var o = t[n] = {exports:{}}; e[n][0].call(o.exports, function(t){var o = e[n][1][t]; return r(o || t)}, o, o.exports)}return t[n].exports}if ("function" == typeof __nr_require)return __nr_require; for (var o = 0; o < n.length; o++)r(n[o]); return r}({1:[function(e, t, n){function r(e, t){return function(){o(e, [(new Date).getTime()].concat(a(arguments)), null, t)}}var o = e("handle"), i = e(2), a = e(3); "undefined" == typeof window.newrelic && (newrelic = NREUM); var u = ["setPageViewName", "addPageAction", "setCustomAttribute", "finished", "addToTrace", "inlineHit"], c = ["addPageAction"], f = "api-"; i(u, function(e, t){newrelic[t] = r(f + t, "api")}), i(c, function(e, t){newrelic[t] = r(f + t)}), t.exports = newrelic, newrelic.noticeError = function(e){"string" == typeof e && (e = new Error(e)), o("err", [e, (new Date).getTime()])}}, {}], 2:[function(e, t, n){function r(e, t){var n = [], r = "", i = 0; for (r in e)o.call(e, r) && (n[i] = t(r, e[r]), i += 1); return n}var o = Object.prototype.hasOwnProperty; t.exports = r}, {}], 3:[function(e, t, n){function r(e, t, n){t || (t = 0), "undefined" == typeof n && (n = e?e.length:0); for (var r = - 1, o = n - t || 0, i = Array(0 > o?0:o); ++r < o; )i[r] = e[t + r]; return i}t.exports = r}, {}], ee:[function(e, t, n){function r(){}function o(e){function t(e){return e && e instanceof r?e:e?u(e, a, i):i()}function n(n, r, o){e && e(n, r, o); for (var i = t(o), a = l(n), u = a.length, c = 0; u > c; c++)a[c].apply(i, r); var s = f[g[n]]; return s && s.push([m, n, r, i]), i}function p(e, t){w[e] = l(e).concat(t)}function l(e){return w[e] || []}function d(e){return s[e] = s[e] || o(n)}function v(e, t){c(e, function(e, n){t = t || "feature", g[n] = t, t in f || (f[t] = [])})}var w = {}, g = {}, m = {on:p, emit:n, get:d, listeners:l, context:t, buffer:v}; return m}function i(){return new r}var a = "nr@context", u = e("gos"), c = e(2), f = {}, s = {}, p = t.exports = o(); p.backlog = f}, {}], gos:[function(e, t, n){function r(e, t, n){if (o.call(e, t))return e[t]; var r = n(); if (Object.defineProperty && Object.keys)try{return Object.defineProperty(e, t, {value:r, writable:!0, enumerable:!1}), r} catch (i){}return e[t] = r, r}var o = Object.prototype.hasOwnProperty; t.exports = r}, {}], handle:[function(e, t, n){function r(e, t, n, r){o.buffer([e], r), o.emit(e, t, n)}var o = e("ee").get("handle"); t.exports = r, r.ee = o}, {}], id:[function(e, t, n){function r(e){var t = typeof e; return!e || "object" !== t && "function" !== t? - 1:e === window?0:a(e, i, function(){return o++})}var o = 1, i = "nr@id", a = e("gos"); t.exports = r}, {}], loader:[function(e, t, n){function r(){if (!w++){var e = v.info = NREUM.info, t = s.getElementsByTagName("script")[0]; if (e && e.licenseKey && e.applicationID && t){c(l, function(t, n){e[t] || (e[t] = n)}); var n = "https" === p.split(":")[0] || e.sslForHttp; v.proto = n?"https://":"http://", u("mark", ["onload", a()], null, "api"); var r = s.createElement("script"); r.src = v.proto + e.agent, t.parentNode.insertBefore(r, t)}}}function o(){"complete" === s.readyState && i()}function i(){u("mark", ["domContent", a()], null, "api")}function a(){return(new Date).getTime()}var u = e("handle"), c = e(2), f = window, s = f.document; NREUM.o = {ST:setTimeout, CT:clearTimeout, XHR:f.XMLHttpRequest, REQ:f.Request, EV:f.Event, PR:f.Promise, MO:f.MutationObserver}, e(1); var p = "" + location, l = {beacon:"bam.nr-data.net", errorBeacon:"bam.nr-data.net", agent:"js-agent.newrelic.com/nr-918.min.js"}, d = window.XMLHttpRequest && XMLHttpRequest.prototype && XMLHttpRequest.prototype.addEventListener && !/CriOS/.test(navigator.userAgent), v = t.exports = {offset:a(), origin:p, features:{}, xhrWrappable:d}; s.addEventListener?(s.addEventListener("DOMContentLoaded", i, !1), f.addEventListener("load", r, !1)):(s.attachEvent("onreadystatechange", o), f.attachEvent("onload", r)), u("mark", ["firstbyte", a()], null, "api"); var w = 0}, {}]}, {}, ["loader"]);</script>
        <link rel="shortcut icon" href="http://blog.worldofwarships.ru/wp-content/themes/warships/favicon.ico" />

        <link rel="alternate" type="application/rss+xml" title="World of Warships Blog &raquo; Лента" href="http://blog.worldofwarships.ru/feed/" />
        <link rel="alternate" type="application/rss+xml" title="World of Warships Blog &raquo; Лента комментариев" href="http://blog.worldofwarships.ru/comments/feed/" />
        <link rel="alternate" type="application/rss+xml" title="World of Warships Blog &raquo; Лента комментариев к &laquo;Главная&raquo;" href="http://blog.worldofwarships.ru/home-page/feed/" />
        <link rel='stylesheet' id='output-css'  href='http://blog.worldofwarships.ru/wp-content/plugins/addthis/css/output.css?ver=3.9.9' type='text/css' media='all' />
        <link rel='stylesheet' id='style-name-css'  href='http://blog.worldofwarships.ru/wp-content/plugins/wg-subscription/css/style.css?ver=3.9.9' type='text/css' media='all' />
        <link rel='stylesheet' id='wpProQuiz_front_style-css'  href='http://blog.worldofwarships.ru/wp-content/plugins/wp-pro-quiz/css/wpProQuiz_front.min.css?ver=0.32' type='text/css' media='all' />
        <link rel='stylesheet' id='css.main-css'  href='http://blog.worldofwarships.ru/wp-content/themes/warships/static/css/style.min.css?ver=1.4.7.5' type='text/css' media='all' />
        <link rel='stylesheet' id='css.gallery-css'  href='http://blog.worldofwarships.ru/wp-content/themes/warships/static/css/blueimp-gallery.min.css?ver=1.4.5' type='text/css' media='all' />
        <link rel='stylesheet' id='css.carousel-css'  href='http://blog.worldofwarships.ru/wp-content/themes/warships/static/css/carousel.css?ver=1.4.5' type='text/css' media='all' />
        <link rel='stylesheet' id='css.font-awesome-css'  href='http://blog.worldofwarships.ru/wp-content/themes/warships/static/fonts/font-awesome/css/font-awesome.min.css?ver=1.4.5' type='text/css' media='all' />
        <link rel='stylesheet' id='adv-spoiler-css'  href='http://blog.worldofwarships.ru/wp-content/plugins/advanced-spoiler/css/advanced-spoiler.css' type='text/css' media='all' />
        <script type='text/javascript' src='http://blog.worldofwarships.ru/wp-includes/js/jquery/jquery.js'></script>
        <script type='text/javascript' src='http://blog.worldofwarships.ru/wp-includes/js/jquery/jquery-migrate.min.js'></script>
        <script type='text/javascript' src='http://blog.worldofwarships.ru/wp-content/plugins/wg-silk-ajax-comments/ajaxcomments.js'></script>
        <script type='text/javascript' src='http://blog.worldofwarships.ru/wp-content/themes/warships/static/js/modernizr.custom.min.js'></script>
        <script type='text/javascript' src='http://blog.worldofwarships.ru/wp-content/plugins/advanced-spoiler/js/jquery-spoiler.js'></script>
        <link rel='shortlink' href='http://blog.worldofwarships.ru/' />
    </head>
    <body class="home page page-id-8 page-template page-template-home-php logged-in custom-background ru_RU singular">
        <a name="top"></a>
        <div id="minWidth">
            <!-- top -->
            <div class="top">
                <header class="header-top">
                    <nav id="access">
                        <div class="menu-primary-container">
                            <ul id="menu-primary" class="menu">
                                <li id="menu-item-4730" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-8 current_page_item menu-item-4730"><a href="http://blog.worldofwarships.ru/">Главная</a></li>
                                <li id="menu-item-7924" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7924"><a href="http://blog.worldofwarships.ru/about-game/">Об игре</a></li>
                                <li id="menu-item-4749" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4749"><a href="http://blog.worldofwarships.ru/authors/">Авторы</a></li>
                                <li id="menu-item-4733" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4733"><a href="http://blog.worldofwarships.ru/faq/">FAQ</a></li>
                                <li id="menu-item-3045" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3045"><a href="http://forum.worldofwarships.ru/">Форум</a></li>
                                <li class="menu-search">
                                    <div class="search_404">
                                        <form method="get" id="searchform" action="http://blog.worldofwarships.ru/">
                                            <input type="text" class="field" name="s" id="s" placeholder="Поиск" />
                                            <span class="search-button-wrap">
                                                <input type="submit" class="submit" name="submit" id="searchsubmit" title="Поиск" value="Поиск">
                                            </span>
                                        </form>
                                    </div>
                                </li>
                                <li id="menu-login" class="no-js dropdown">
                                    <span class="wrap-user-login">
                                        <a target='_blank' class="user-login" href="http://wargaming.net/personal/" title="">
                                            <span class="text">4ydjk</span><span class="login-nav-arrow"></span>
                                        </a>
                                            
                                    </span>
                                    <ul>
                                        <li class="user-profile">
                                            <a target="_blank" href="http://wargaming.net/personal/" title="">
                                                <span class="wgnet"></span>
                                                <span class="arr">Личный кабинет</span>
                                            </a>
                                        </li>
                                  
                                        <li>
                                            <?php
                                        
                                            Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form'])
                                            . Html::submitButton(
                                                'Logout (' . Yii::$app->user->identity->username . ')',
                                                ['class' => 'btn btn-link']
                                            )
                                            . Html::endForm() 
                                            ?>
                                            <a href="https://blog.worldofwarships.ru/wp-login.php?action=logout&amp;_wpnonce=3c0d2a0e01" title="Выход">
                                                <span>Выход</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </header>
            </div>
            
            <div class="b-logo-top">
                <a href="<?php echo yii\helpers\Url::to(['']); ?>">
                    <img src="http://blog.worldofwarships.ru/wp-content/themes/warships/static/img/head_img_ru_RU.png">
                </a>
            </div>
            
            <div class="inner">
                <!-- middle -->
                <div class="b-mainmiddle">
                    <div class="b-mainmiddle-i">
                        <div class="b-content-table">
                            <div class="l-col-left">
                                <div class="newsblock_header">
                                    <div class="news_h2_and_current_filter">
                                        <h2 class="s-title s-title-4 newsblock_header">
                                            <!--<i class="b-ico ico-1"></i>-->
                                            <i class="h-icon fa fa-pencil-square-o"></i>Сообщения и обсуждения
                                            <a target="_blank" href="/feed"
                                                <img src="http://blog.worldofwarships.ru/wp-content/themes/warships/static/img/rss_feed.png" alt="подписаться на rss">
                                            </a>
                                        </h2>
                                        <div id="current_primary_filter" class="current_primary_filter" style="display: none;" >
                                            <span class="message"></span>
                                            <span id='primary_filter_value' class="primary_filter_value">
                                                <span class="value"></span>
                                                <a id="cancel_filter" href="/">
                                                    <img src="http://blog.worldofwarships.ru/wp-content/themes/warships/static/img/cancel_tag.png"
                                                         title="">
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="news_filters">
                                        <a  href="/?filter=newest" data-filter-name="filter" data-filter-value="newest">последние</a>
                                        <a  href="/?filter=popular" data-filter-name="filter" data-filter-value="popular">популярные</a>
                                    </div>
                                    <div class="news_filters_time">
                                        <a class="disabled-filter" href="#" data-filter-name="filter" data-filter-value="alltime">за всё время</a>
                                        <a  href="/?filter=month" data-filter-name="filter" data-filter-value="month">за месяц</a>
                                        <a  href="/?filter=week" data-filter-name="filter" data-filter-value="week">за неделю</a>
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <div style="display: none;" id="preloader" class="preloader">
                                    Загрузка<br>
                                    <img src="http://blog.worldofwarships.ru/wp-content/themes/warships/static/img/preloader_logo.gif" class="">
                                </div>
                                <div id="posts_wrap"> 
                                    <ul id="post_previews" class="b-listvacancy">
                                        <li style="left: 0px; top: 0px">
                                            <h3><a href="http://blog.worldofwarships.ru/test-sovetskaya-vetka/">Тест «Советская ветка. Проверь себя!»</a></h3>
                                            <p class="tile_time_author">30.03.16,                    <span class="author_post_title" data-author-title=""
                                                                                                           data-author-slug="komanda-razrabotchikov"><a href="/authors#komanda-razrabotchikov">Команда разработчиков</a></span>
                                            </p>
                                            <a class="preview-img-link" href="http://blog.worldofwarships.ru/test-sovetskaya-vetka/"><img width="239" height="135" src="http://blog.worldofwarships.ru/wp-content/uploads/sites/4/2016/03/WG_SPB_WoWS_Web_TestPreview_711x400-239x135.jpg" class="attachment-post-thumbnail wp-post-image" alt="WG_SPB_WoWS_Web_TestPreview_711x400" /></a>
                                            <a href="http://blog.worldofwarships.ru/test-sovetskaya-vetka/"><p class="tile_description">Мы предлагаем вам проверить, насколько хорошо вы успели изучить корабли отечественного флота в игре и реальности. 
                                                </p></a>
                                            <p class="tile_tags">
                                                <span class="tile_views">47 465</span><a href="http://blog.worldofwarships.ru/test-sovetskaya-vetka/#comment-list"><span class="tile_comments">164</span></a>
                                            </p>
                                        </li>
                                        <li style="left: 0px; top: 0px">
                                            <h3><a href="http://blog.worldofwarships.ru/damage-system1/">О модели повреждений. Часть 1</a></h3>
                                            <p class="tile_time_author">03.11.15,                    <span class="author_post_title" data-author-title=""
                                                                                                           data-author-slug="filipp-molodkovets"><a href="/authors#filipp-molodkovets">Филипп Молодковец</a>, Supertest Manager</span>
                                            </p>
                                            <a class="preview-img-link" href="http://blog.worldofwarships.ru/damage-system1/"><img width="243" height="135" src="http://blog.worldofwarships.ru/wp-content/uploads/sites/4/2015/11/WoWs_Damage3_720x400-243x135.jpg" class="attachment-post-thumbnail wp-post-image" alt="WoWs_Damage3_720x400" /></a>
                                            <a href="http://blog.worldofwarships.ru/damage-system1/"><p class="tile_description">Модель повреждений  — один из самых сложных компонентов игры. Эта статья будет интересна не только начинающим командирам, но и старожилам World of Warships.
                                                </p></a>
                                            <p class="tile_tags">
                                                Теги:                                                    <a href="http://blog.worldofwarships.ru/tag/vooruzhenie/" data-filter-name="tag" data-filter-value="vooruzhenie" data-filter-title="Вооружение">
                                                    Вооружение                            </a>
                                                ,                                                    <a href="http://blog.worldofwarships.ru/tag/gameplay/" data-filter-name="tag" data-filter-value="gameplay" data-filter-title="Геймплей">
                                                    Геймплей                            </a>
                                                ,                                                    <a href="http://blog.worldofwarships.ru/tag/team/" data-filter-name="tag" data-filter-value="team" data-filter-title="Команда">
                                                    Команда                            </a>
                                                ,                                                    <a href="http://blog.worldofwarships.ru/tag/novichkam/" data-filter-name="tag" data-filter-value="novichkam" data-filter-title="Новичкам">
                                                    Новичкам                            </a>
                                                <span class="tile_views">42 469</span><a href="http://blog.worldofwarships.ru/damage-system1/#comment-list"><span class="tile_comments">91</span></a>
                                            </p>
                                        </li>
                                        <li style="left: 0px; top: 0px">
                                            <h3><a href="http://blog.worldofwarships.ru/new-capabilities/">Обновление портала. Дополнительные возможности</a></h3>
                                            <p class="tile_time_author">30.10.15,                    <span class="author_post_title" data-author-title=""
                                                                                                           data-author-slug="komanda-razrabotchikov"><a href="/authors#komanda-razrabotchikov">Команда разработчиков</a></span>
                                            </p>
                                            <a class="preview-img-link" href="http://blog.worldofwarships.ru/new-capabilities/"><img width="239" height="135" src="http://blog.worldofwarships.ru/wp-content/uploads/sites/4/2015/10/social_bonus-239x135.png" class="attachment-post-thumbnail wp-post-image" alt="social_bonus" /></a>
                                            <a href="http://blog.worldofwarships.ru/new-capabilities/"><p class="tile_description">На этой неделе вышло обновление нашего портала. Теперь у читателей появились дополнительные возможности. Узнайте подробности!
                                                </p></a>
                                            <p class="tile_tags">
                                                Теги:                                                    <a href="http://blog.worldofwarships.ru/tag/obt/" data-filter-name="tag" data-filter-value="obt" data-filter-title="ОБТ">
                                                    ОБТ                            </a>
                                                <span class="tile_views">14 243</span><a href="http://blog.worldofwarships.ru/new-capabilities/#comment-list"><span class="tile_comments">78</span></a>
                                            </p>
                                        </li>
                                        <li style="left: 0px; top: 0px">
                                            <h3><a href="http://blog.worldofwarships.ru/ships-naming/">Выбор имени. Как назвать корабль?</a></h3>
                                            <p class="tile_time_author">29.10.15,                    <span class="author_post_title" data-author-title=""
                                                                                                           data-author-slug="komanda-razrabotchikov"><a href="/authors#komanda-razrabotchikov">Команда разработчиков</a></span>
                                            </p>
                                            <a class="preview-img-link" href="http://blog.worldofwarships.ru/ships-naming/"><img width="243" height="135" src="http://blog.worldofwarships.ru/wp-content/uploads/sites/4/2015/10/720h400_ship_name2-243x135.jpg" class="attachment-post-thumbnail wp-post-image" alt="720х400_ship_name2" /></a>
                                            <a href="http://blog.worldofwarships.ru/ships-naming/"><p class="tile_description">Как происходит подбор имени для «бумажных» кораблей? Узнайте на примере ветки германского флота.
                                                </p></a>
                                            <p class="tile_tags">
                                                <span class="tile_views">22 718</span><a href="http://blog.worldofwarships.ru/ships-naming/#comment-list"><span class="tile_comments">75</span></a>
                                            </p>
                                        </li>
                                        <li style="left: 0px; top: 0px">
                                            <h3><a href="http://blog.worldofwarships.ru/voroshilov_battery/">Безмолвный страж. Ворошиловская батарея</a></h3>
                                            <p class="tile_time_author">22.10.15,                    <span class="author_post_title" data-author-title=""
                                                                                                           data-author-slug="komanda-razrabotchikov"><a href="/authors#komanda-razrabotchikov">Команда разработчиков</a></span>
                                            </p>
                                            <a class="preview-img-link" href="http://blog.worldofwarships.ru/voroshilov_battery/"><img width="241" height="135" src="http://blog.worldofwarships.ru/wp-content/uploads/sites/4/2015/10/history_blog1-241x135.jpg" class="attachment-post-thumbnail wp-post-image" alt="history_blog" /></a>
                                            <a href="http://blog.worldofwarships.ru/voroshilov_battery/"><p class="tile_description">Присоединяйтесь к путешествию по Ворошиловской батарее!
                                                </p></a>
                                            <p class="tile_tags">
                                                <span class="tile_views">8 223</span><a href="http://blog.worldofwarships.ru/voroshilov_battery/#comment-list"><span class="tile_comments">17</span></a>
                                            </p>
                                        </li>
                                    </ul>
                                    <div id='pagination' data-max-pages="84"
                                         data-current-page="1"
                                         class='pagination dotted-top'>
                                        <p>
                                            <a href="http://blog.worldofwarships.ru/?page=2"><i class="next_page"> </i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <div class="l-col-right">

                                <h2 class="s-title s-title-4">
                                    <i class="b-ico ico-subscribe"></i>Подписаться    </h2>
                                <div class="wg-subscribe dotted-bottom">
                                    <form method="post" id="wg_subscriptionform" action="http://blog.worldofwarships.ru/">
                                        <input type="hidden" name="wg_subscription_user_email" value="kuchura.d@yandex.ru"/>
                                        <input type="submit" class="submit" name="submit" id="wg_subscriptionsubmit" value="Подписаться" />
                                    </form>
                                </div>
                                <h2 class="s-title s-title-4">
                                    <i class="h-icon fa fa-volume-up"></i>Главные темы    </h2>
                                <div class="tags_list">
                                    <img src="http://blog.worldofwarships.ru/wp-content/themes/warships/static/img/blog-quote.png" class="tags_quote">
                                    <p>
                                        <a  href="http://blog.worldofwarships.ru/tag/download/" data-filter-name="tag" data-filter-value="download"
                                            data-filter-title="Cкачать игру">Cкачать игру</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/carriers/" data-filter-name="tag" data-filter-value="carriers"
                                            data-filter-title="Авианосцы">Авианосцы</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/aktsiya/" data-filter-name="tag" data-filter-value="aktsiya"
                                            data-filter-title="Акция">Акция</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/alpha/" data-filter-name="tag" data-filter-value="alpha"
                                            data-filter-title="Альфа-тест">Альфа-тест</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/beta/" data-filter-name="tag" data-filter-value="beta"
                                            data-filter-title="Бета-тест">Бета-тест</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/video/" data-filter-name="tag" data-filter-value="video"
                                            data-filter-title="Видео">Видео</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/viktorina/" data-filter-name="tag" data-filter-value="viktorina"
                                            data-filter-title="Викторина">Викторина</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/vooruzhenie/" data-filter-name="tag" data-filter-value="vooruzhenie"
                                            data-filter-title="Вооружение">Вооружение</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/gameplay/" data-filter-name="tag" data-filter-value="gameplay"
                                            data-filter-title="Геймплей">Геймплей</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/graphics/" data-filter-name="tag" data-filter-value="graphics"
                                            data-filter-title="Графика">Графика</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/game/" data-filter-name="tag" data-filter-value="game"
                                            data-filter-title="Игра">Игра</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/interview/" data-filter-name="tag" data-filter-value="interview"
                                            data-filter-title="Интервью">Интервью</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/infografika/" data-filter-name="tag" data-filter-value="infografika"
                                            data-filter-title="Инфографика">Инфографика</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/istoriya/" data-filter-name="tag" data-filter-value="istoriya"
                                            data-filter-title="История">История</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/team/" data-filter-name="tag" data-filter-value="team"
                                            data-filter-title="Команда">Команда</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/quiz/" data-filter-name="tag" data-filter-value="quiz"
                                            data-filter-title="Конкурс">Конкурс</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/cruisers/" data-filter-name="tag" data-filter-value="cruisers"
                                            data-filter-title="Крейсеры">Крейсеры</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/battleships/" data-filter-name="tag" data-filter-value="battleships"
                                            data-filter-title="Линкоры">Линкоры</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/morehodka/" data-filter-name="tag" data-filter-value="morehodka"
                                            data-filter-title="мореходка">мореходка</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/novichkam/" data-filter-name="tag" data-filter-value="novichkam"
                                            data-filter-title="Новичкам">Новичкам</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/obt/" data-filter-name="tag" data-filter-value="obt"
                                            data-filter-title="ОБТ">ОБТ</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/upgrade/" data-filter-name="tag" data-filter-value="upgrade"
                                            data-filter-title="Прокачка">Прокачка</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/tactics/" data-filter-name="tag" data-filter-value="tactics"
                                            data-filter-title="Тактика">Тактика</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/expedition/" data-filter-name="tag" data-filter-value="expedition"
                                            data-filter-title="Экспедиция">Экспедиция</a>
                                        <a  href="http://blog.worldofwarships.ru/tag/e-smintsy/" data-filter-name="tag" data-filter-value="e-smintsy"
                                            data-filter-title="Эсминцы">Эсминцы</a>
                                    </p>
                                </div>
                                <h2 class="s-title s-title-4 dotted-top">
                                    <i class="h-icon join-us"></i>Присоединяйтесь    </h2>
                                <script type="text/javascript" src="http://vk.com/js/api/openapi.js?105"></script>
                                <!-- VK Widget -->
                                <div id="vk_groups"></div>
                                <script type="text/javascript">
            VK.Widgets.Group("vk_groups", {mode: 0, width: "245", height: "400", color1: '393434', color2: 'FFFFFF', color3: '5B7FA6'}, 29468395);
                                </script>
                                <div id="fb-root"></div>
                                <script type="text/javascript">
                                    (function(d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id)) return;
                                    js = d.createElement(s); js.id = id;
                                    js.src = "//connect.facebook.net/ru_RU/all.js#xfbml=1&appId=378262445667037";
                                    fjs.parentNode.insertBefore(js, fjs);
                                    }(document, 'script', 'facebook-jssdk'));
                                </script>
                                <!-- FB Widget -->
                                <div id="fb_like_box">
                                    <fb:like-box href="http://www.facebook.com/WorldOfWarships" width="245" colorscheme="dark" show_faces="true" header="true" stream="false" show_border="false"></fb:like-box>
                                </div>
                                <ul class="b-socials" style="margin-top: 10px;">
                                    <li><a class="soc_link" href="https://www.facebook.com/WorldOfWarships"><i class="ico_soc_fb"></i></a>
                                    <li><a class="soc_link" href="https://vk.com/worldofwarships"><i class="ico_soc_vk"></i></a>
                                    <li><a class="soc_link" href="https://twitter.com/warships_ru"><i class="ico_soc_tw"></i></a>
                                </ul>
                                <div style="clear:both;"></div>
                                <h2 class="s-title s-title-4 dotted-top"><i class="h-icon fa fa-comment-o"></i>Форум</h2>
                                <div class="b-box-shadow-content">
                                    <ul class="b-forumlist">
                                        <li>
                                            <div class="b-news-important">
                                                <div>
                                                    <a href="http://forum.worldofwarships.ru/index.php?/topic/60426-%D0%BD%D0%B0%D1%80%D1%83%D1%88%D0%B5%D0%BD%D0%B8%D0%B5-%D0%BF%D1%80%D0%B0%D0%B2%D0%B8%D0%BB-%D0%B8%D0%B3%D1%80%D1%8B-%D0%B8-%D0%BD%D0%B0%D0%BA%D0%B0%D0%B7%D0%B0%D0%BD%D0%B8%D0%B5-%D0%B2%D0%B8%D0%BD%D0%BE%D0%B2%D0%BD%D1%8B%D1%85-25-%D0%B8%D1%8E%D0%BB/" class="b-forum-theme">
                                                        Нарушение Правил игры и наказание виновных 25 июля                                </a>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="b-news-important">
                                                <div>
                                                    <a href="http://forum.worldofwarships.ru/index.php?/topic/60425-%D1%85%D1%83-%D0%B8%D0%B7-%D1%85%D1%83%D1%81-%D0%B4%D0%BD%D1%91%D0%BC-%D1%80%D0%BE%D0%B6%D0%B4%D0%B5%D0%BD%D0%B8%D1%8F-%D0%B6%D0%B5%D0%BD%D0%B0-%D1%8F%D0%B7%D0%B0%D0%B1%D0%B0%D0%BD-%D0%B8%D1%81%D0%BA%D1%83%D1%81%D1%81%D1%82%D0%B2/" class="b-forum-theme">
                                                        Ху из Ху...с днём рождения Жена !!! #язабан #искусство не забанить                                </a>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="b-news-important">
                                                <div>
                                                    <a href="http://forum.worldofwarships.ru/index.php?/topic/60422-%D0%B4%D0%BD%D0%B5%D0%B2%D0%BD%D0%B8%D0%BA%D0%B8-%D0%BA%D0%B0%D0%BF%D0%B8%D1%82%D0%B0%D0%BD%D1%81%D0%BA%D0%BE%D0%B3%D0%BE-%D0%BA%D0%BE%D1%82%D0%B0-3/" class="b-forum-theme">
                                                        дневники капитанского кота #3                                </a>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="b-news-important">
                                                <div>
                                                    <a href="http://forum.worldofwarships.ru/index.php?/topic/60409-%D0%B8%D1%82%D0%BE%D0%B3%D0%B8-%D0%BE%D0%B1%D1%89%D0%B5%D0%B3%D0%BE-%D1%82%D0%B5%D1%81%D1%82%D0%B0-059-%D0%BA%D0%BE%D0%BC%D0%BC%D0%B5%D0%BD%D1%82%D0%B0%D1%80%D0%B8%D0%B9/" class="b-forum-theme">
                                                        Итоги общего теста 0.5.9. [Комментарий]                                </a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <!--b-mainmiddle-bd-->
                    </div>
                    <!--b-mainmiddle-i-->
                    <script type='text/javascript'>
                        jQuery(document).ready(function () {
                        if (wowsblog) {
                        wowsblog.PostLoaderInstance = new wowsblog.PostLoaderClass({
                        name: '',
                                value: '',
                                title: ''
                        }, [
                        { name: 'filter', value: '' },
                        { name: 'page', value: '1' }
                        ], {
                        tag: {message: 'Другие публикации с этим тегом', title: 'Отменить фильтрацию по тегу'},
                                author: {message: 'Другие статьи этого автора', title: 'Отменить фильтрацию по автору'}
                        }
                        );
                        }
                        });
                    </script>
                    <!-- /middle -->
                    <!-- middle -->
                </div>
                <!-- inner -->
            </div>
            <!-- bottom -->

            <div class="b-bottom b-bottom_main_page">
                <div class="hr_line"></div>
                <div class="copyright_bottom">
                    <a title="Wargaming.net" class="logo_footer" href="http://wargaming.net/"></a>
                    <div class="copyright_bottom-txt">
                        &copy; 2009-2016 Wargaming.net &reg;. Все права защищены                            </div>
                </div>
            </div>
        </div>

        <a href="#top" title="" id="scroll-to-top" class="no-js"><div><img src="http://blog.worldofwarships.ru/wp-content/themes/warships/static/img/to_top_arrow.png" alt=""><span>Наверх</span></div></a>

        <!-- Google Tag Manager -->
        <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=GTM-MS4S6"
                height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({'gtm.start': new Date().getTime(), event: 'gtm.js'});
            var f = d.getElementsByTagName(s)[0],
                    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                    '//www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', 'GTM-MS4S6');</script>
        <!-- End Google Tag Manager -->

        <script type="text/javascript">panopress.imagebox();</script>
        <!--Plugin WP Missed Schedule 2013.0730.7777 Active - Tag d76ca983e4e0b1146b8511f40cd66015-->

        <script type='text/javascript' src='http://blog.worldofwarships.ru/wp-content/themes/warships/static/js/posts_loader.min.js?ver=1.4.5'></script>
        <script type='text/javascript' src='http://blog.worldofwarships.ru/wp-content/themes/warships/static/js/jquery.placeholder.min.js?ver=1.4.5'></script>
        <script type='text/javascript' src='http://blog.worldofwarships.ru/wp-content/themes/warships/static/js/jquery.simplePagination.min.js?ver=1.4.5'></script>
        <script type='text/javascript' src='http://blog.worldofwarships.ru/wp-content/themes/warships/static/js/jquery.mousewheel.min.js?ver=1.4.5'></script>
        <script type='text/javascript' src='http://blog.worldofwarships.ru/wp-content/themes/warships/static/js/js.min.js?ver=1.4.5'></script>
        <script type='text/javascript' src='http://blog.worldofwarships.ru/wp-includes/js/comment-reply.min.js?ver=3.9.9'></script>
        <!--wp_footer--><div id="background-image">
            <img src="/wp-content/themes/warships/static/img/release_bckg_blog.jpg?v=1.4.9.1" alt="" style="margin-left: -990px !important;">
        </div>
        <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
            <div class="slides"></div>
            <h3 class="title"></h3>
            <a class="prev">‹</a>
            <a class="next">›</a>
            <a class="close">×</a>
            <a class="play-pause"></a>
            <ol class="indicator"></ol>
        </div>
        <script type='text/javascript' src='http://blog.worldofwarships.ru/wp-content/themes/warships/static/js/jquery.blueimp-gallery.min.js'></script>
        <script type="text/javascript">window.NREUM || (NREUM = {}); NREUM.info = {"beacon":"bam.nr-data.net", "licenseKey":"e9cee6cd82", "applicationID":"16185927", "transactionName":"AC0MBTUwJgIyMkBZBDlnLzIzOCcffDlbXQ4=", "queueTime":0, "applicationTime":1050, "atts":"LGoPc30qNQw=", "errorBeacon":"bam.nr-data.net", "agent":""}</script>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
